---
title: 'Introduction'
avatar: './me.jpg'
skills:
  - Java
  - C
  - C++
  - GO
  - Python
  - Keras/PyTorch/MXNet
  - Erlang
  - Lisp
  - HTML/CSS
  - React
  - Node.js
  - Gatsby
---

Hello, I'm Leo, a software engineer based in UK, with a deep love of computers, programming, mathematics and problem solving. I have engineered numerous elegant solutions for back-end systems and DevOps pipelines. My work concerns robust, maintainable, high-performance code for complex, difficult-to-reason-about problems, often revolving around huge legacy code-bases, layered technologies and operationally rich enterprises.

Shortly after graduating from [University of Nottingham](https://www.nottingham.ac.uk/computerscience/), I joined the software-engineering team at [EXEL](https://www.exel.co.uk/) where I work on a wide variety of impactful and stimulating projects each day of my professional life.

Please follow links at bottom left for social code/media. View my writing at [Academia.edu](https://independent.academia.edu/ljnwriting), and my creative work at [Tumblr](https://ljn-create.tumblr.com/)

Please see below for an overview of languages and frameworks I have worked in:
