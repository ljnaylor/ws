---
title: 'Hello world, welcome, my name is:'
name: 'Leo J. Naylor'
subtitle: 'Software Engineer'
contactText: 'Contact'
---

I'm a software engineer based in the UK specializing in designing, and building clean, maintainable software systems that scale effectively.
