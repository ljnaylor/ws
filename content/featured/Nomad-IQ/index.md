---
date: '3'
title: 'Nomad-IQ'
cover: './NomadIQ.png'
github: 'https://github.com/red-eyed-tree-frog/nomad-iq'
tech:
  - Java
  - Maven
  - Gradle
showInProjects: true
---
AI control structures and design patterns for autonomous-vehicle/robotic control systems built on classical subsumptive logic archetypes, for single and collaborative entities i.e. Agent Design.
