---
date: '1'
title: 'Sparrowhawk - PHP CRUD'
cover: './SparrowHawk.png'
github: 'https://github.com/red-eyed-tree-frog/sparrowhawk'
tech:
  - PHP
  - HTML
  - Bootstrap
  - JS
  - CSS
  - SQL
showInProjects: true
---

A simple CRUD records control system running on phpmyadmin/WAMP, one of my first forays into web/database development.
