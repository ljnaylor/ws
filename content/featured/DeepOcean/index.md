---
date: '3'
title: 'Deep Ocean'
cover: './DeepOcean.png'
github: 'https://github.com/red-eyed-tree-frog/DeepOcean'
external: 'https://youtube.com/'
tech:
  - Python
  - PyGame
  - Adobe Suites
  - Ableton Live
showInProjects: true
---

Beautifully styled adventure diving game for PC + MAC. The game concept combines themes of classic 2D scrolling games like Zool with more modern mobile apps like Flappy Bird. The result is a smooth, challenging game experience with hand-coded physics and hand-crafted art, sound and music that make it a memorable project to have worked on! [PyGame](https://www.pygame.org/news), [AbobeCC](https://www.adobe.com/uk/creativecloud.html?gclid=CjwKCAjwhOD0BRAQEiwAK7JHmEv4FE7DYWmp5ZcvQHVHsX760jH1N3dIpHJRrZA7_jRHuT93dVpH2RoCq_UQAvD_BwE&sdid=88X75SKR&mv=search&ef_id=CjwKCAjwhOD0BRAQEiwAK7JHmEv4FE7DYWmp5ZcvQHVHsX760jH1N3dIpHJRrZA7_jRHuT93dVpH2RoCq_UQAvD_BwE:G:s&s_kwcid=AL!3085!3!273769967928!e!!g!!adobe%20cc), [Ableton](https://www.ableton.com/en/)
