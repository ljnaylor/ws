---
date: '4'
title: 'j5 - Robotic Vision'
cover: './roboeyepic.png'
github: 'https://bitbucket.org/ljnaylor/j5/src/master/'
tech:
  - C++
  - Python
  - Berkeley Autolabs
  - DexNet
  - GQCNN
  - ROS (Robot Operating System)
  - OpenCV
  - VTK
  - CUDA
showInProjects: true
---

Deep-learned robotic grasping and computer vision with 3D-object-tracking algorithms. Using the Berkeley Autolabs open-source software as a starting point, this project helped me to consolidate a strong understanding of research frameworks used for robotics. The grasp-quality convolutional neural networks which form the heart of Dex-Net/GQCNN are run in a deep reinforcement learning simulated environment, to which an extra control layer using 3d object-detection algorithms is added. This approach strikes a balance between (1) the generalisational ability of neural networks and (2) the precise recognition of known objects stored as triangulated meshes, in order to generate a 'best of both worlds' approach to robotic grasping.
