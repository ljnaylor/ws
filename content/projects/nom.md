---
date: '4'
title: 'Nomad-IQ'
cover: './NomadIQ.png'
github: 'https://github.com/red-eyed-tree-frog/nomad-iq'
tech:
  - Java
  - Maven
  - Gradle
showInProjects: true
---
Classical AI control structures and design patterns for autonomous-vehicle/robotic control systems built on classical AI archetypes i.e. Agent Design.
