---
date: '7'
title: 'Cumulus - Cloud Update'
cover: './Cumulus.png'
github: 'https://github.com/red-eyed-tree-frog/cumulus'
tech:
  - Java
  - Python
  - WildFly / RedHat
  - Flask
  - Cloudy
showInProjects: true
---

A secure, cloud-deployed, commerical update update server system. Providing clients with larger, domain-deployed web-apps an automated passive update service with robust back-end software for performance critical, integral applications. [WildFly](https://wildfly.org/), and [Flask](https://palletsprojects.com/p/flask/).
