---
date: '2020'
title: 'A commercially deployed application'
github: ''
external: 'https://www.pagecontaininginfo.com'
ios: 'https://apps.apple.com/us/LOCATION'
android: 'https://play.google.com/store/apps/LLOCATION'
tech:
  - Lang
  - iOS
  - Android
company: 'self'
showInProjects: false
---
