---
date: '1'
title: 'Fizmed'
cover: './FizMed.png'
github: 'https://github.com/red-eyed-tree-frog/fizmed'
tech:
  - R
  - FuzzyR
showInProjects: true
---
Medical diagnosis in R using fuzzy-inference-systems.
