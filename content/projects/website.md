---
date: '8'
title: 'Website'
cover: './website.png'
github: 'https://bitbucket.org/ljnaylor/ws/src/master/'
tech:
  - JS
  - React
  - Node
  - GraphQL
  - Gatsby
  - Google Analytics
  - Styled Components
  - CSS
showInProjects: true
---

Personal website built with trendy web technologies.
