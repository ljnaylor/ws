---
date: '6'
title: 'Eclipse IDE, IBM® RTC plug-in'
cover: './Eclipse.png'
github: 'https://github.com/red-eyed-tree-frog/eclipsertcplugin'
external: 'https://octoprofile.now.sh'
tech:
  - Java
  - IBM® (EWM)
  - RTC
  - Jazz
  - Eclipse
showInProjects: true
---

Developed plugins for the Eclipse Java Enterprise IDE/ IBM Jazz RTC source control systems, automating systems and database maintenance pertaining to code delivery and database model integrity. [Eclipse Java](https://www.eclipse.org/), [RTC](https://jazz.net/library/article/1000) and [IBM](https://jazz.net/)
