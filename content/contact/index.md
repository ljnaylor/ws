---
title: 'Get in touch via e-mail'
---

This site's purpose is to provide new contacts with an overview/profile of me and my work, but if you have further questions relating to programming, or wish to discuss a project, please mail me and I will endeavour to respond promptly.
