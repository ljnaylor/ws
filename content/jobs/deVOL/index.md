---
date: '2015'
title: 'Craftsman'
company: 'deVOL'
location: 'East Midlands, UK'
range: '2015 - 2017'
url: 'https://www.devolkitchens.co.uk/'
---

- Trained as an artisanal woodworker in traditional carpentry techniques,
  cabinetmaking, furniture and interior-joinery, providing services to
  numerous established and highly regarded contractors in the EastMidlands region. During this period of time I also acquired skills in draughtsmanship and CNC programming, whilst studying luthiery in my spare time. 
