---
date: '2012'
title: 'ERP Software Consultancy Intern'
company: 'TCS John Huxley'
location: 'Staffordshire, UK'
range: '2012 - 2013'
url: 'https://www.tcsjohnhuxley.com/'
---

- Supervised the install + adoption of an MS Dynamics AX based
  accountancy/ERP system alongside consultant accountancy software
  specialist [Russell Lawrence](https://www.linkedin.com/in/russell-lawrence-21426a5/?originalSubdomain=uk). The clients were TCS John-Huxley, a large
  manufacturing company based in Staffordshire, exporting internationally
