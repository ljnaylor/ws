---
date: '2013'
title: 'Sound Engineer'
company: 'Freelance'
location: 'London, UK'
range: '2013 - 2015'
url: 'https://www.london.gov.uk/events/'
---

- Worked on events at venues across Central London including The 02 Arena,
  Kensington Olympia, ITV Studios, Winter Wonderland, Soho
  House, NYE Fireworks, Paternoster Sq.
