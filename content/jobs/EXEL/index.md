---
date: '2018'
title: 'Software Engineer'
company: 'EXEL'
location: 'Nottingham, UK'
range: '2018 - 2020'
url: 'https://www.exel.co.uk/'
---

- Writing clean, reusable, maintainable JAVA EE 8/11 code to an Agile
  methodology for a diverse range of internal systems projects
- Working with a variety of different languages, frameworks and sourcecontrol systems e.g. JPA, Hibernate, Spring, Maven, Gradle, RTC-JAZZ,
  ANT, Python, WildFly & RedHat server technologies, IBM ELM
- Development lead on security/operationally critical projects: server
  penetration testing, redesigning commercial client update systems
- Developed in house tools for code-quality analytics
- Communicates and collaborates with multi-disciplinary teams of engineers
  and stakeholders
- Providing robust performance metrics, testing distributed systems at scale
