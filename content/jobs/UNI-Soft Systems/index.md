---
date: '2017'
title: 'Intern'
company: 'UNI-Soft Systems'
location: 'Nottingham University - Jubilee Campus'
range: 'Spring 2018'
url: 'https://uni-soft.co.uk/'
---

- Interned with UNI-Soft Systems exploring the use of PyTorch on some of their data contracts
- Researched deep-learned natural language processing techniques to be integrated with company IP
