import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { email } from '@config';
import styled from 'styled-components';
import { theme, mixins, media, Section } from '@styles';
import TextLoop from 'react-text-loop';
import { createGlobalStyle } from 'styled-components';

const { colors, fontSizes, fonts } = theme;

const StyledContainer = styled(Section)`
  ${mixins.flexCenter};
  flex-direction: column;
  align-items: flex-start;
  min-height: 100vh;
  ${media.tablet`padding-top: 150px;`};
  div {
    width: 100%;
  }
`;
const StyledOverline = styled.h1`
  color: ${colors.slate};
  display: flex;
  justify-content: left;
  font-size: 20px;
  font-weight: bold;
  font-family: monospace;
  overflow: hidden; /* Ensures the content is not revealed until the animation */
  border-right: .15em solid orange; /* The typwriter cursor */
  white-space: nowrap; /* Keeps the content on a single line */
  margin: 0; /* Gives that scrolling effect as the typing happens */
  letter-spacing: .15em; /* Adjust as needed */
  animation: 
    typing 1.5s steps(30, end),
    blink-caret .5s step-end infinite;
    @keyframes typing {
      from { width: 0 }
      to { width: 70% }
    }
    
    /* The typewriter cursor effect */
    @keyframes blink-caret {
      from, to { border-color: transparent }
      50% { border-color: orange }
    }
    justify-content: left;
  ${media.desktop`font-size: ${fontSizes.sm};`};
  ${media.tablet`font-size: ${fontSizes.smish};`};
`;
const StyledTitle = styled.h2`
  font-size: 80px;
  line-height: 1.1;
  margin: 0;
  padding-above: 40px;
  ${media.desktop`font-size: 70px;`};
  ${media.tablet`font-size: 60px;`};
  ${media.phablet`font-size: 50px;`};
  ${media.phone`font-size: 40px;`};
`;
const StyledSubtitle = styled.h3`
  font-size: 60px;
  line-height: 1.1;
  color: ${colors.slate};
  ${media.desktop`font-size: 70px;`};
  ${media.tablet`font-size: 60px;`};
  ${media.phablet`font-size: 50px;`};
  ${media.phone`font-size: 40px;`};
`;
const StyledDescription = styled.div`
  margin-top: 25px;
  width: 50%;
  max-width: 500px;
  a {
    ${mixins.inlineLink};
  }
`;
const StyledEmailLink = styled.a`
  ${mixins.bigButton};
  margin-top: 100px;
`;

const Hero = ({ data }) => {
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    const timeout = setTimeout(() => setIsMounted(true), 1000);
    return () => clearTimeout(timeout);
  }, []);

  const { frontmatter, html } = data[0].node;

  const one = () => (
    <StyledOverline style={{ transitionDelay: '25ms' }}>{frontmatter.title}</StyledOverline>
  );
  const two = () => (
    <StyledTitle style={{ transitionDelay: '2000ms' }}>{frontmatter.name}</StyledTitle>
  );
  const three = () => (
    <StyledSubtitle style={{ transitionDelay: '2300ms' }}>{frontmatter.subtitle}</StyledSubtitle>
  );
  const four = () => (
    <StyledDescription
      style={{ transitionDelay: '2500ms' }}
      dangerouslySetInnerHTML={{ __html: html }}
    />
  );

  const five = () => (
    <div style={{ transitionDelay: '2500ms' }}>
      <StyledEmailLink href={`mailto:${email}`}>
        <h2>
          <TextLoop
            springConfig={{
              stiffness: 180,
              damping: 20,
              noWrap: false,
              mask: true,
              adjustingSpeed: 50,
            }}>
            <span>Contact me</span>
            <span>Susisiek su manimi</span>
            <span>اتصل بي</span>
            <span>Mənimlə əlaqə saxlayın</span>
            <span>Звяжыцеся са мной</span>
            <span>Свържи се с мен</span>
            <span>আমার সাথে যোগাযোগ কর</span>
            <span>Kontaktiraj me</span>
            <span>Contacta’m</span>
            <span>Kontaka ako</span>
            <span>Kontaktujte mě</span>
            <span>Cysylltwch â mi</span>
            <span>Kontakt mig</span>
            <span>Kontaktiere mich</span>
            <span>Επικοινώνησε μαζί μου</span>
            <span>Contáctame</span>
            <span>Võta minuga ühendust</span>
            <span>Jar zaitez nirekin harremanetan</span>
            <span>با من تماس بگیر</span>
            <span>Ota yhteyttä minuun</span>
            <span>Contactez moi</span>
            <span>Teagmháil a dhéanamh liom</span>
            <span>Póñase en contacto comigo</span>
            <span>મારો સંપર્ક કરો</span>
            <span>Tuntube ni</span>
            <span>मुझसे संपर्क करो</span>
            <span>Contact me</span>
            <span>Kontaktiraj me</span>
            <span>Kontakte mwen</span>
            <span>Keress meg</span>
            <span>Կապվեք ինձ հետ</span>
            <span>Hubungi saya</span>
            <span>Kpoo m</span>
            <span>Hafðu samband við mig</span>
            <span>Contattami</span>
            <span> צור איתי קשר</span>
            <span> 私に連絡して</span>
            <span>Hubungi kula</span>
            <span>ამიკავშირდი</span>
            <span>Менімен хабарлас</span>
            <span>ទាក់ទងមកខ្ញុំ</span>
            <span>ನನ್ನನ್ನು ಸಂಪರ್ಕಿಸಿ</span>
            <span>저에게 연락</span>
            <span>Contattami</span>
            <span>ຕິດຕໍຂ້ອຍ</span>
            <span>Sazinies ar mani</span>
            <span>Mifandraisa amiko</span>
            <span>Whakawhiti mai</span>
            <span>Контактирај ме</span>
            <span>എന്നെ ബന്ധപ്പെടുക</span>
            <span>Надтай холбогдоорой</span>
            <span>मला संपर्क करा</span>
            <span>Hubungi saya</span>
            <span>Ikkuntattjani</span>
            <span>ငါ့ကိုဆက်သွယ်ပါ</span>
            <span>मलाई सम्पर्क गर्नुस्</span>
            <span>Kontakt meg</span>
            <span>Nditumizireni</span>
            <span>ਮੇਰੇ ਨਾਲ ਸੰਪਰਕ ਕਰੋ</span>
            <span>Skontaktuj się ze mną</span>
            <span>Contate-me</span>
            <span>Contacteaza-ma</span>
            <span>Свяжитесь со мной</span>
            <span>මව සම්බන්ධ කරගන්න</span>
            <span>Kontaktujte ma</span>
            <span>Kontaktiraj me</span>
            <span>Ila soo xiriir</span>
            <span>Me kontakto</span>
            <span>Контактирај ме</span>
            <span>Nthomelle</span>
            <span>Taroskeun ka kuring</span>
            <span>Kontakta mig</span>
            <span>Wasiliana nami</span>
            <span>என்னை தொடர்பு கொள்</span>
            <span>నన్ను సంప్రదించండి</span>
            <span>Бо ман тамос гиред</span>
            <span>ติดต่อฉัน</span>
            <span>Tawagan mo ako</span>
            <span>Benimle irtibata geçin</span>
            <span> مجھ سے رابطہ کریں</span>
            <span>Liên hệ với tôi</span>
            <span>קאנטאקט מיר</span>
            <span>Kan si mi</span>
            <span>联络我</span>
            <span>联络我</span>
            <span>聯絡我</span>
            <span>Ngithinte</span>
          </TextLoop>{' '}
        </h2>
        <TextLoop>
          <span>
            "Blessed is the man, who having nothing to say, abstains from giving wordy evidence of the fact."
          <br />
            <i> -George Eliot</i>
          </span>
          <span>
            "The art of conversation is the art of hearing as well as of being heard."
          <br />
            <i> -William Hazlitt</i>
          </span>
          <span>
            "Conversation is a meeting of minds with different memories and habits."
            <br />
            <i> -Theodore Zeldin</i>
          </span>
          <span>
            "One good conversation can shift the direction of change forever."{' '}
            <br />
            <i> -Linda Lambert</i>
          </span>
          <span>
            "It was impossible to get a conversation going, everybody was talking too much."{' '}
            <br />
            <i> -Yogi berra</i>
          </span>
          <span>
            "Good conversation is as stimulating as black coffee, and just as hard to sleep after."
            <br />
            <i> -Anne Morrow Lindbergh</i>
          </span>
          <span>
            "Say the right thing at the right place, leave unsaid the wrong thing at the tempting moment."
            <br />
            <i> -Dorothy Nevill</i>
          </span>
          <span>
            "Conversation is a catalyst for innovation."
            <br />
            <i> -John Seely Brown</i>
          </span>
        </TextLoop>
      </StyledEmailLink>
    </div>
  );

  const items = [one, two, three, four, five];

  return (
    <StyledContainer>
      <TransitionGroup component={null}>
        {isMounted &&
          items.map((item, i) => (
            <CSSTransition key={i} classNames="fadeup" timeout={3000}>
              {item}
            </CSSTransition>
          ))}
      </TransitionGroup>
    </StyledContainer>
  );
};

Hero.propTypes = {
  data: PropTypes.array.isRequired,
};

export default Hero;
