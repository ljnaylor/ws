import React from 'react';

const IconLoader = () => (
  <React.Fragment>
    <div id="particles-background" className="vertical-centered-box"></div>
    <div id="particles-foreground" className="vertical-centered-box"></div>

    <div className="vertical-centered-box">
      <div className="content">
        <div className="loader-circle"></div>
        <div className="loader-line-mask">
          <div className="loader-line"></div>
        </div>
        <svg
          id="logo"
          width="150.11506347656248px"
          height="132.83749999999998px"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="149.94246826171877 8.581250000000011 200.11506347656248 132.83749999999998"
          data-darkreader-inline-bgimage=""
          data-darkreader-inline-bgcolor=""
          preserveAspectRatio="xMidYMid">
          <defs>
            <linearGradient
              id="editing-glowing-gradient"
              x1="0.8280295144952539"
              x2="0.17197048550474614"
              y1="0.8773547901113858"
              y2="0.12264520988861416">
              <stop offset="0" stopColor="#43ff9d"></stop>
              <stop offset="1" stopColor="#90ffff"></stop>
            </linearGradient>
            <filter id="editing-glowing" x="-100%" y="-100%" width="300%" height="300%">
              <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="13.5"></feGaussianBlur>
              <feMerge>
                <feMergeNode in="blur"></feMergeNode>
                <feMergeNode in="SourceGraphic"></feMergeNode>
              </feMerge>
            </filter>
          </defs>
          <g filter="url(#editing-glowing)">
            <g transform="translate(183.78000259399414, 99.85000038146973)">
              <path
                stroke="#64FFDA"
                strokeWidth="5"
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M7.70 0L7.70 0Q7.00 0 6.55-0.46L6.55-0.46L6.55-0.46Q6.09-0.91 6.09-1.61L6.09-1.61L6.09-47.39L6.09-47.39Q6.09-48.09 6.55-48.55L6.55-48.55L6.55-48.55Q7.00-49.00 7.70-49.00L7.70-49.00L11.41-49.00L11.41-49.00Q12.11-49.00 12.57-48.55L12.57-48.55L12.57-48.55Q13.02-48.09 13.02-47.39L13.02-47.39L13.02-6.09L36.26-6.09L36.26-6.09Q37.03-6.09 37.49-5.64L37.49-5.64L37.49-5.64Q37.94-5.18 37.94-4.41L37.94-4.41L37.94-1.61L37.94-1.61Q37.94-0.91 37.45-0.46L37.45-0.46L37.45-0.46Q36.96 0 36.26 0L36.26 0L7.70 0ZM60.06 0.70L60.06 0.70Q52.43 0.70 47.39-3.08L47.39-3.08L47.39-3.08Q42.35-6.86 42.07-14.14L42.07-14.14L42.07-14.14Q42.07-14.77 42.49-15.23L42.49-15.23L42.49-15.23Q42.91-15.68 43.61-15.68L43.61-15.68L47.39-15.68L47.39-15.68Q48.86-15.68 49.21-14.07L49.21-14.07L49.21-14.07Q49.63-9.59 52.64-7.42L52.64-7.42L52.64-7.42Q55.65-5.25 60.20-5.25L60.20-5.25L60.20-5.25Q65.45-5.25 68.29-8.72L68.29-8.72L68.29-8.72Q71.12-12.18 71.12-18.06L71.12-18.06L71.12-42.91L46.27-42.91L46.27-42.91Q45.57-42.91 45.12-43.37L45.12-43.37L45.12-43.37Q44.66-43.82 44.66-44.52L44.66-44.52L44.66-47.39L44.66-47.39Q44.66-48.09 45.12-48.55L45.12-48.55L45.12-48.55Q45.57-49.00 46.27-49.00L46.27-49.00L76.44-49.00L76.44-49.00Q77.21-49.00 77.67-48.55L77.67-48.55L77.67-48.55Q78.12-48.09 78.12-47.32L78.12-47.32L78.12-17.92L78.12-17.92Q78.12-9.38 73.33-4.34L73.33-4.34L73.33-4.34Q68.53 0.70 60.06 0.70L60.06 0.70ZM91.49 0L91.49 0Q90.79 0 90.34-0.46L90.34-0.46L90.34-0.46Q89.88-0.91 89.88-1.61L89.88-1.61L89.88-47.32L89.88-47.32Q89.88-48.09 90.34-48.55L90.34-48.55L90.34-48.55Q90.79-49.00 91.49-49.00L91.49-49.00L94.71-49.00L94.71-49.00Q95.97-49.00 96.46-48.02L96.46-48.02L119.70-12.32L119.70-47.32L119.70-47.32Q119.70-48.09 120.16-48.55L120.16-48.55L120.16-48.55Q120.61-49.00 121.31-49.00L121.31-49.00L124.67-49.00L124.67-49.00Q125.44-49.00 125.90-48.55L125.90-48.55L125.90-48.55Q126.35-48.09 126.35-47.32L126.35-47.32L126.35-1.68L126.35-1.68Q126.35-0.98 125.90-0.49L125.90-0.49L125.90-0.49Q125.44 0 124.74 0L124.74 0L121.38 0L121.38 0Q120.33 0 119.70-0.98L119.70-0.98L96.53-36.33L96.53-1.61L96.53-1.61Q96.53-0.91 96.04-0.46L96.04-0.46L96.04-0.46Q95.55 0 94.85 0L94.85 0L91.49 0Z"
                fill="url(#editing-glowing-gradient)"></path>
            </g>
          </g>
          <style>text {}</style>
          <style className="darkreader darkreader--sync" media="screen"></style>
        </svg>
      </div>
    </div>
  </React.Fragment>
);

export default IconLoader;
