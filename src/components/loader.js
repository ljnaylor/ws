import React, { useState, useEffect } from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import anime from 'animejs';
import styled from 'styled-components';
import { theme, mixins } from '@styles';

const { colors } = theme;

const StyledContainer = styled.div`
  ${mixins.flexCenter};
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 99;
`;
const StyledLogo = styled.div`
  width: max-content;
  max-width: 100px;
  transition: ${theme.transition};
  opacity: ${props => (props.isMounted ? 1 : 0)};
  svg {
    width: 100%;
    height: 100%;
    display: block;
    margin: 0 auto;
    fill: none;
    user-select: none;
    #B {
      opacity: 0;
    }
  }
`;

const Loader = ({ finishLoading }) => {
  const animate = () => {
    const loader = anime.timeline({
      complete: () => finishLoading(),
    });

    loader
      .add({
        targets: '#logo path',
        delay: 500,
        duration: 1000,
        easing: 'easeInOutQuart',
        strokeDashoffset: [anime.setDashoffset, 0],
      })
      .add({
        targets: '#logo #B',
        duration: 1000,
        easing: 'easeInOutQuart',
        opacity: 1,
      })
      .add({
        targets: '#logo',
        delay: 1300,
        duration: 300,
        easing: 'easeInOutQuart',
        opacity: 0,
        scale: 0.1,
      })
      .add({
        targets: '.loader',
        duration: 200,
        easing: 'easeInOutQuart',
        opacity: 0,
        zIndex: -1,
      });
  };

  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    !(function(a, b) {
      'use strict';
      function c(a) {
        a = a || {};
        for (let b = 1; b < arguments.length; b++) {
          const c = arguments[b];
          if (c) {
            for (const d in c) {
              c.hasOwnProperty(d) &&
                ('object' === typeof c[d] ? deepExtend(a[d], c[d]) : (a[d] = c[d]));
            }
          }
        }
        return a;
      }
      function d(d, g) {
        function h() {
          if (y) {
            (r = b.createElement('canvas')),
            (r.className = 'pg-canvas'),
            (r.style.display = 'block'),
            d.insertBefore(r, d.firstChild),
            (s = r.getContext('2d')),
            i();
            for (let c = Math.round((r.width * r.height) / g.density), e = 0; c > e; e++) {
              const f = new n();
              f.setStackPos(e), z.push(f);
            }
            a.addEventListener(
              'resize',
              function() {
                k();
              },
              !1,
            ),
            b.addEventListener(
              'mousemove',
              function(a) {
                (A = a.pageX), (B = a.pageY);
              },
              !1,
            ),
            D &&
                !C &&
                a.addEventListener(
                  'deviceorientation',
                  function() {
                    (F = Math.min(Math.max(-event.beta, -30), 30)),
                    (E = Math.min(Math.max(-event.gamma, -30), 30));
                  },
                  !0,
                ),
            j(),
            q('onInit');
          }
        }
        function i() {
          (r.width = d.offsetWidth),
          (r.height = d.offsetHeight),
          (s.fillStyle = g.dotColor),
          (s.strokeStyle = g.lineColor),
          (s.lineWidth = g.lineWidth);
        }
        function j() {
          if (y) {
            (u = a.innerWidth), (v = a.innerHeight), s.clearRect(0, 0, r.width, r.height);
            for (var b = 0; b < z.length; b++) {z[b].updatePosition();}
            for (var b = 0; b < z.length; b++) {z[b].draw();}
            G || (t = requestAnimationFrame(j));
          }
        }
        function k() {
          i();
          for (var a = d.offsetWidth, b = d.offsetHeight, c = z.length - 1; c >= 0; c--) {(z[c].position.x > a || z[c].position.y > b) && z.splice(c, 1);}
          const e = Math.round((r.width * r.height) / g.density);
          if (e > z.length) {
            for (; e > z.length; ) {
              const f = new n();
              z.push(f);
            }
          } else {e < z.length && z.splice(e);}
          for (c = z.length - 1; c >= 0; c--) {z[c].setStackPos(c);}
        }
        function l() {
          G = !0;
        }
        function m() {
          (G = !1), j();
        }
        function n() {
          switch (
            (this.stackPos,
            (this.active = !0),
            (this.layer = Math.ceil(3 * Math.random())),
            (this.parallaxOffsetX = 0),
            (this.parallaxOffsetY = 0),
            (this.position = {
              x: Math.ceil(Math.random() * r.width),
              y: Math.ceil(Math.random() * r.height),
            }),
            (this.speed = {}),
            g.directionX)
          ) {
            case 'left':
              this.speed.x = +(-g.maxSpeedX + Math.random() * g.maxSpeedX - g.minSpeedX).toFixed(2);
              break;
            case 'right':
              this.speed.x = +(Math.random() * g.maxSpeedX + g.minSpeedX).toFixed(2);
              break;
            default:
              (this.speed.x = +(-g.maxSpeedX / 2 + Math.random() * g.maxSpeedX).toFixed(2)),
              (this.speed.x += this.speed.x > 0 ? g.minSpeedX : -g.minSpeedX);
          }
          switch (g.directionY) {
            case 'up':
              this.speed.y = +(-g.maxSpeedY + Math.random() * g.maxSpeedY - g.minSpeedY).toFixed(2);
              break;
            case 'down':
              this.speed.y = +(Math.random() * g.maxSpeedY + g.minSpeedY).toFixed(2);
              break;
            default:
              (this.speed.y = +(-g.maxSpeedY / 2 + Math.random() * g.maxSpeedY).toFixed(2)),
              (this.speed.x += this.speed.y > 0 ? g.minSpeedY : -g.minSpeedY);
          }
        }
        function o(a, b) {
          return b ? void (g[a] = b) : g[a];
        }
        function p() {
          console.log('destroy'),
          r.parentNode.removeChild(r),
          q('onDestroy'),
          f && f(d).removeData(`plugin_${  e}`);
        }
        function q(a) {
          void 0 !== g[a] && g[a].call(d);
        }
        let r;
        let s;
        let t;
        let u;
        let v;
        let w;
        let x;
        var y = !!b.createElement('canvas').getContext;
        var z = [];
        var A = 0;
        var B = 0;
        var C = !navigator.userAgent.match(
          /(iPhone|iPod|iPad|Android|BlackBerry|BB10|mobi|tablet|opera mini|nexus 7)/i,
        );
        var D = !!a.DeviceOrientationEvent;
        var E = 0;
        var F = 0;
        var G = !1;
        return (
          (g = c({}, a[e].defaults, g)),
          (n.prototype.draw = function() {
            s.beginPath(),
            s.arc(
              this.position.x + this.parallaxOffsetX,
              this.position.y + this.parallaxOffsetY,
              g.particleRadius / 2,
              0,
              2 * Math.PI,
              !0,
            ),
            s.closePath(),
            s.fill(),
            s.beginPath();
            for (let a = z.length - 1; a > this.stackPos; a--) {
              const b = z[a];
              const c = this.position.x - b.position.x;
              const d = this.position.y - b.position.y;
              const e = Math.sqrt(c * c + d * d).toFixed(2);
              e < g.proximity &&
                (s.moveTo(
                  this.position.x + this.parallaxOffsetX,
                  this.position.y + this.parallaxOffsetY,
                ),
                g.curvedLines
                  ? s.quadraticCurveTo(
                    Math.max(b.position.x, b.position.x),
                    Math.min(b.position.y, b.position.y),
                    b.position.x + b.parallaxOffsetX,
                    b.position.y + b.parallaxOffsetY,
                  )
                  : s.lineTo(b.position.x + b.parallaxOffsetX, b.position.y + b.parallaxOffsetY));
            }
            s.stroke(), s.closePath();
          }),
          (n.prototype.updatePosition = function() {
            if (g.parallax) {
              if (D && !C) {
                const a = (u - 0) / 60;
                w = (E - -30) * a + 0;
                const b = (v - 0) / 60;
                x = (F - -30) * b + 0;
              } else {(w = A), (x = B);}
              (this.parallaxTargX = (w - u / 2) / (g.parallaxMultiplier * this.layer)),
              (this.parallaxOffsetX += (this.parallaxTargX - this.parallaxOffsetX) / 10),
              (this.parallaxTargY = (x - v / 2) / (g.parallaxMultiplier * this.layer)),
              (this.parallaxOffsetY += (this.parallaxTargY - this.parallaxOffsetY) / 10);
            }
            const c = d.offsetWidth;
            const e = d.offsetHeight;
            switch (g.directionX) {
              case 'left':
                this.position.x + this.speed.x + this.parallaxOffsetX < 0 &&
                  (this.position.x = c - this.parallaxOffsetX);
                break;
              case 'right':
                this.position.x + this.speed.x + this.parallaxOffsetX > c &&
                  (this.position.x = 0 - this.parallaxOffsetX);
                break;
              default:
                (this.position.x + this.speed.x + this.parallaxOffsetX > c ||
                  this.position.x + this.speed.x + this.parallaxOffsetX < 0) &&
                  (this.speed.x = -this.speed.x);
            }
            switch (g.directionY) {
              case 'up':
                this.position.y + this.speed.y + this.parallaxOffsetY < 0 &&
                  (this.position.y = e - this.parallaxOffsetY);
                break;
              case 'down':
                this.position.y + this.speed.y + this.parallaxOffsetY > e &&
                  (this.position.y = 0 - this.parallaxOffsetY);
                break;
              default:
                (this.position.y + this.speed.y + this.parallaxOffsetY > e ||
                  this.position.y + this.speed.y + this.parallaxOffsetY < 0) &&
                  (this.speed.y = -this.speed.y);
            }
            (this.position.x += this.speed.x), (this.position.y += this.speed.y);
          }),
          (n.prototype.setStackPos = function(a) {
            this.stackPos = a;
          }),
          h(),
          { option: o, destroy: p, start: m, pause: l }
        );
      }
      var e = 'particleground';
      var f = a.jQuery;
      (a[e] = function(a, b) {
        return new d(a, b);
      }),
      (a[e].defaults = {
        minSpeedX: 0.1,
        maxSpeedX: 0.7,
        minSpeedY: 0.1,
        maxSpeedY: 0.7,
        directionX: 'center',
        directionY: 'center',
        density: 1e4,
        dotColor: '#666666',
        lineColor: '#666666',
        particleRadius: 7,
        lineWidth: 1,
        curvedLines: !1,
        proximity: 100,
        parallax: !0,
        parallaxMultiplier: 5,
        onInit: function() {},
        onDestroy: function() {},
      }),
      f &&
          (f.fn[e] = function(a) {
            if ('string' === typeof arguments[0]) {
              let b;
              const c = arguments[0];
              const g = Array.prototype.slice.call(arguments, 1);
              return (
                this.each(function() {
                  f.data(this, `plugin_${  e}`) &&
                    'function' === typeof f.data(this, `plugin_${  e}`)[c] &&
                    (b = f.data(this, `plugin_${  e}`)[c].apply(this, g));
                }),
                void 0 !== b ? b : this
              );
            }
            return 'object' !== typeof a && a
              ? void 0
              : this.each(function() {
                f.data(this, `plugin_${  e}`) || f.data(this, `plugin_${  e}`, new d(this, a));
              });
          });
    })(window, document),
    (function() {
      for (
        var a = 0, b = ['ms', 'moz', 'webkit', 'o'], c = 0;
        c < b.length && !window.requestAnimationFrame;
        ++c
      ) {
        (window.requestAnimationFrame = window[`${b[c]  }RequestAnimationFrame`]),
        (window.cancelAnimationFrame =
              window[`${b[c]  }CancelAnimationFrame`] ||
              window[`${b[c]  }CancelRequestAnimationFrame`]);
      }
      window.requestAnimationFrame ||
          (window.requestAnimationFrame = function(b) {
            const c = new Date().getTime();
            const d = Math.max(0, 16 - (c - a));
            const e = window.setTimeout(function() {
              b(c + d);
            }, d);
            return (a = c + d), e;
          }),
      window.cancelAnimationFrame ||
            (window.cancelAnimationFrame = function(a) {
              clearTimeout(a);
            });
    })();

    particleground(document.getElementById('particles-foreground'), {
      dotColor: 'rgba(255, 255, 255, 1)',
      lineColor: 'rgba(255, 255, 255, 0.05)',
      minSpeedX: 0.3,
      maxSpeedX: 0.6,
      minSpeedY: 0.3,
      maxSpeedY: 0.6,
      density: 50000, // One particle every n pixels
      curvedLines: false,
      proximity: 250, // How close two dots need to be before they join
      parallaxMultiplier: 10, // Lower the number is more extreme parallax
      particleRadius: 4, // Dot size
    });

    particleground(document.getElementById('particles-background'), {
      dotColor: 'rgba(255, 255, 255, 0.5)',
      lineColor: 'rgba(255, 255, 255, 0.05)',
      minSpeedX: 0.075,
      maxSpeedX: 0.15,
      minSpeedY: 0.075,
      maxSpeedY: 0.15,
      density: 30000, // One particle every n pixels
      curvedLines: false,
      proximity: 20, // How close two dots need to be before they join
      parallaxMultiplier: 20, // Lower the number is more extreme parallax
      particleRadius: 2, // Dot size
    });
    const timeout = setTimeout(() => setIsMounted(true), 10);
    animate();
    return () => clearTimeout(timeout);
  }, []);

  return (
    //<div className={containerStyles.loaderGalaxy}>
    <div className="Loader">
      <div id="particles-background" className="vertical-centered-box"></div>
      <div id="particles-foreground" className="vertical-centered-box"></div>

      <div className="vertical-centered-box">
        <StyledContainer className="loader">
          <Helmet bodyAttributes={{ class: `hidden` }} />

          <StyledLogo isMounted={isMounted}>
            <div className="content">
              <div className="loader-circle"></div>
              <div className="loader-line-mask">
                <div className="loader-line"></div>
              </div>
              {
                /* <svg id="logo" width="150.11506347656248px" height="132.83749999999998px" xmlns="http://www.w3.org/2000/svg" viewBox="149.94246826171877 8.581250000000011 200.11506347656248 132.83749999999998" data-darkreader-inline-bgimage="" data-darkreader-inline-bgcolor="" preserveAspectRatio="xMidYMid"><defs><linearGradient id="editing-glowing-gradient" x1="0.8280295144952539" x2="0.17197048550474614" y1="0.8773547901113858" y2="0.12264520988861416"><stop offset="0" stop-color="#43ff9d"></stop><stop offset="1" stop-color="#90ffff"></stop></linearGradient><filter id="editing-glowing" x="-100%" y="-100%" width="300%" height="300%"><feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="13.5"></feGaussianBlur><feMerge><feMergeNode in="blur"></feMergeNode><feMergeNode in="SourceGraphic"></feMergeNode></feMerge></filter></defs><g filter="url(#editing-glowing)"><g transform="translate(183.78000259399414, 99.85000038146973)"><path stroke="#64FFDA"
                  strokeWidth="5"
                  strokeLinecap="round"
                  strokeLinejoin="round" d="M7.70 0L7.70 0Q7.00 0 6.55-0.46L6.55-0.46L6.55-0.46Q6.09-0.91 6.09-1.61L6.09-1.61L6.09-47.39L6.09-47.39Q6.09-48.09 6.55-48.55L6.55-48.55L6.55-48.55Q7.00-49.00 7.70-49.00L7.70-49.00L11.41-49.00L11.41-49.00Q12.11-49.00 12.57-48.55L12.57-48.55L12.57-48.55Q13.02-48.09 13.02-47.39L13.02-47.39L13.02-6.09L36.26-6.09L36.26-6.09Q37.03-6.09 37.49-5.64L37.49-5.64L37.49-5.64Q37.94-5.18 37.94-4.41L37.94-4.41L37.94-1.61L37.94-1.61Q37.94-0.91 37.45-0.46L37.45-0.46L37.45-0.46Q36.96 0 36.26 0L36.26 0L7.70 0ZM60.06 0.70L60.06 0.70Q52.43 0.70 47.39-3.08L47.39-3.08L47.39-3.08Q42.35-6.86 42.07-14.14L42.07-14.14L42.07-14.14Q42.07-14.77 42.49-15.23L42.49-15.23L42.49-15.23Q42.91-15.68 43.61-15.68L43.61-15.68L47.39-15.68L47.39-15.68Q48.86-15.68 49.21-14.07L49.21-14.07L49.21-14.07Q49.63-9.59 52.64-7.42L52.64-7.42L52.64-7.42Q55.65-5.25 60.20-5.25L60.20-5.25L60.20-5.25Q65.45-5.25 68.29-8.72L68.29-8.72L68.29-8.72Q71.12-12.18 71.12-18.06L71.12-18.06L71.12-42.91L46.27-42.91L46.27-42.91Q45.57-42.91 45.12-43.37L45.12-43.37L45.12-43.37Q44.66-43.82 44.66-44.52L44.66-44.52L44.66-47.39L44.66-47.39Q44.66-48.09 45.12-48.55L45.12-48.55L45.12-48.55Q45.57-49.00 46.27-49.00L46.27-49.00L76.44-49.00L76.44-49.00Q77.21-49.00 77.67-48.55L77.67-48.55L77.67-48.55Q78.12-48.09 78.12-47.32L78.12-47.32L78.12-17.92L78.12-17.92Q78.12-9.38 73.33-4.34L73.33-4.34L73.33-4.34Q68.53 0.70 60.06 0.70L60.06 0.70ZM91.49 0L91.49 0Q90.79 0 90.34-0.46L90.34-0.46L90.34-0.46Q89.88-0.91 89.88-1.61L89.88-1.61L89.88-47.32L89.88-47.32Q89.88-48.09 90.34-48.55L90.34-48.55L90.34-48.55Q90.79-49.00 91.49-49.00L91.49-49.00L94.71-49.00L94.71-49.00Q95.97-49.00 96.46-48.02L96.46-48.02L119.70-12.32L119.70-47.32L119.70-47.32Q119.70-48.09 120.16-48.55L120.16-48.55L120.16-48.55Q120.61-49.00 121.31-49.00L121.31-49.00L124.67-49.00L124.67-49.00Q125.44-49.00 125.90-48.55L125.90-48.55L125.90-48.55Q126.35-48.09 126.35-47.32L126.35-47.32L126.35-1.68L126.35-1.68Q126.35-0.98 125.90-0.49L125.90-0.49L125.90-0.49Q125.44 0 124.74 0L124.74 0L121.38 0L121.38 0Q120.33 0 119.70-0.98L119.70-0.98L96.53-36.33L96.53-1.61L96.53-1.61Q96.53-0.91 96.04-0.46L96.04-0.46L96.04-0.46Q95.55 0 94.85 0L94.85 0L91.49 0Z" fill="url(#editing-glowing-gradient)"></path></g></g><style>text {

                  }</style><style class="darkreader darkreader--sync" media="screen"></style></svg> */

                <svg
                  id="logo"
                  width="150.11506347656248px"
                  height="132.83749999999998px"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="149.94246826171877 8.581250000000011 200.11506347656248 132.83749999999998"
                  data-darkreader-inline-bgimage=""
                  data-darkreader-inline-bgcolor=""
                  preserveAspectRatio="xMidYMid">
                  <defs>
                    <linearGradient
                      id="editing-glowing-gradient"
                      x1="0.8280295144952539"
                      x2="0.17197048550474614"
                      y1="0.8773547901113858"
                      y2="0.12264520988861416">
                      <stop offset="0" stopColor="#43ff9d"></stop>
                      <stop offset="1" stopColor="#90ffff"></stop>
                    </linearGradient>
                    <filter id="editing-glowing" x="-100%" y="-100%" width="300%" height="300%">
                      <feGaussianBlur
                        in="SourceGraphic"
                        result="blur"
                        stdDeviation="13.5"></feGaussianBlur>
                      <feMerge>
                        <feMergeNode in="blur"></feMergeNode>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                      </feMerge>
                    </filter>
                  </defs>
                  <g filter="url(#editing-glowing)">
                    <g transform="translate(183.78000259399414, 99.85000038146973)">
                      <path
                        stroke="#64FFDA"
                        strokeWidth="5"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M7.70 0L7.70 0Q7.00 0 6.55-0.46L6.55-0.46L6.55-0.46Q6.09-0.91 6.09-1.61L6.09-1.61L6.09-47.39L6.09-47.39Q6.09-48.09 6.55-48.55L6.55-48.55L6.55-48.55Q7.00-49.00 7.70-49.00L7.70-49.00L11.41-49.00L11.41-49.00Q12.11-49.00 12.57-48.55L12.57-48.55L12.57-48.55Q13.02-48.09 13.02-47.39L13.02-47.39L13.02-6.09L36.26-6.09L36.26-6.09Q37.03-6.09 37.49-5.64L37.49-5.64L37.49-5.64Q37.94-5.18 37.94-4.41L37.94-4.41L37.94-1.61L37.94-1.61Q37.94-0.91 37.45-0.46L37.45-0.46L37.45-0.46Q36.96 0 36.26 0L36.26 0L7.70 0ZM60.06 0.70L60.06 0.70Q52.43 0.70 47.39-3.08L47.39-3.08L47.39-3.08Q42.35-6.86 42.07-14.14L42.07-14.14L42.07-14.14Q42.07-14.77 42.49-15.23L42.49-15.23L42.49-15.23Q42.91-15.68 43.61-15.68L43.61-15.68L47.39-15.68L47.39-15.68Q48.86-15.68 49.21-14.07L49.21-14.07L49.21-14.07Q49.63-9.59 52.64-7.42L52.64-7.42L52.64-7.42Q55.65-5.25 60.20-5.25L60.20-5.25L60.20-5.25Q65.45-5.25 68.29-8.72L68.29-8.72L68.29-8.72Q71.12-12.18 71.12-18.06L71.12-18.06L71.12-42.91L46.27-42.91L46.27-42.91Q45.57-42.91 45.12-43.37L45.12-43.37L45.12-43.37Q44.66-43.82 44.66-44.52L44.66-44.52L44.66-47.39L44.66-47.39Q44.66-48.09 45.12-48.55L45.12-48.55L45.12-48.55Q45.57-49.00 46.27-49.00L46.27-49.00L76.44-49.00L76.44-49.00Q77.21-49.00 77.67-48.55L77.67-48.55L77.67-48.55Q78.12-48.09 78.12-47.32L78.12-47.32L78.12-17.92L78.12-17.92Q78.12-9.38 73.33-4.34L73.33-4.34L73.33-4.34Q68.53 0.70 60.06 0.70L60.06 0.70ZM91.49 0L91.49 0Q90.79 0 90.34-0.46L90.34-0.46L90.34-0.46Q89.88-0.91 89.88-1.61L89.88-1.61L89.88-47.32L89.88-47.32Q89.88-48.09 90.34-48.55L90.34-48.55L90.34-48.55Q90.79-49.00 91.49-49.00L91.49-49.00L94.71-49.00L94.71-49.00Q95.97-49.00 96.46-48.02L96.46-48.02L119.70-12.32L119.70-47.32L119.70-47.32Q119.70-48.09 120.16-48.55L120.16-48.55L120.16-48.55Q120.61-49.00 121.31-49.00L121.31-49.00L124.67-49.00L124.67-49.00Q125.44-49.00 125.90-48.55L125.90-48.55L125.90-48.55Q126.35-48.09 126.35-47.32L126.35-47.32L126.35-1.68L126.35-1.68Q126.35-0.98 125.90-0.49L125.90-0.49L125.90-0.49Q125.44 0 124.74 0L124.74 0L121.38 0L121.38 0Q120.33 0 119.70-0.98L119.70-0.98L96.53-36.33L96.53-1.61L96.53-1.61Q96.53-0.91 96.04-0.46L96.04-0.46L96.04-0.46Q95.55 0 94.85 0L94.85 0L91.49 0Z"
                        fill="url(#editing-glowing-gradient)"></path>
                    </g>
                  </g>
                  <style>text {}</style>
                  <style className="darkreader darkreader--sync" media="screen"></style>
                </svg>
              }
            </div>
          </StyledLogo>
        </StyledContainer>
      </div>
    </div>
    //</div>
  );
};

Loader.propTypes = {
  finishLoading: PropTypes.func.isRequired,
};

export default Loader;
