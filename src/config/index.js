module.exports = {
  siteTitle: 'Leo Naylor | Software Engineer',
  siteDescription:
    'Leo Naylor is a software engineer from the UK, who specializes in developing exceptional software with a variety of languages and platforms',
  siteKeywords:
    'Leo Naylor, Leo, Naylor, ljn, software engineer, back-end engineer, Java, data-science',
  siteUrl: 'https://leonaylor.com',
  siteLanguage: 'en_UK',
  googleAnalyticsID: 'UA-164008069-1',
  googleVerification: '',
  name: 'Leo Naylor',
  location: 'UK',
  email: 'leo.jmn@gmail.com',
  github: 'https://github.com/red-eyed-tree-frog',
  twitterHandle: '@leonaylor',
  socialMedia: [
    {
      name: 'GitHub',
      url: 'https://github.com/red-eyed-tree-frog',
    },
    {
      name: 'Linkedin',
      url: 'https://www.linkedin.com/in/leo-naylor-2281a514a/',
    },
    {
      name: 'Twitter',
      url: 'https://twitter.com/leonaylor',
    },
  ],

  navLinks: [
    {
      name: 'Introduction',
      url: '/#about',
    },
    {
      name: 'Professional Experience',
      url: '/#jobs',
    },
    {
      name: 'Portfolio',
      url: '/#projects',
    },
    {
      name: 'Contact',
      url: '/#contact',
    }
  ],

  navHeight: 100,
  greenColor: '#64ffda',
  navyColor: '#0a192f',
  darkNavyColor: '#020c1b',

  srConfig: (delay = 200) => ({
    origin: 'bottom',
    distance: '20px',
    duration: 500,
    delay,
    rotate: { x: 0, y: 0, z: 0 },
    opacity: 0,
    scale: 1,
    easing: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
    mobile: true,
    reset: false,
    useDelay: 'always',
    viewFactor: 0.25,
    viewOffset: { top: 0, right: 0, bottom: 0, left: 0 },
  }),
};
